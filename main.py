import pygame
from pygame.locals import *
import time

from renderer import *
from board import Board


def simulate():
    WINDOW_SIZE = (640, 640)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    board = Board()

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("Jeu de dames")
    mainClock = pygame.time.Clock()

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    board.playerClicked(mouse_pos)

        if not pause:
            pass

        if frameCount % 1 == 0:
            renderBoard(window, board)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()
