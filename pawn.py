
class Pawn:

    def __init__(self, p_player, p_pawn_type):
        self.__player = p_player
        self.__type = p_pawn_type
        self.__selected = False

    def getPlayer(self):
        return self.__player

    def setPlayer(self, p_new_player):
        self.__player = p_new_player

    def getType(self):
        return self.__type

    def setType(self, p_new_type):
        self.__type = p_new_type

    def setSelected(self, value):
        self.__selected = value

    def isSelected(self):
        return self.__selected

    @classmethod
    def getPawnByPlayer(cls, p_pawn_list, p_player):
        pass

    @classmethod
    def resetSelection(cls, p_grid):
        for y in range(len(p_grid)):
            for x in range(len(p_grid[0])):
                square = p_grid[y][x]
                if square is not None:
                    if square.isSelected():
                        square.setSelected(False)
