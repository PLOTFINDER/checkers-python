import pygame
from BasicVector import Vec2


def renderBoard(p_window, p_board):
    square_size = p_board.getSquareSize()
    grid_size = p_board.getGridSize()
    grid = p_board.getGrid()
    color_odd = (255, 255, 255)
    color_even = (0, 0, 0)
    color_player_one = (255, 0, 0)
    color_player_two = (0, 0, 255)
    color_pawn_selected = (0, 255, 0)

    renderGrid(p_window, grid_size, square_size, color_odd, color_even)
    renderPawns(p_window, grid, grid_size, square_size, color_player_one, color_player_two, color_pawn_selected)
    renderMoves(p_window, p_board, square_size, color_pawn_selected)


def renderGrid(p_window, p_grid_size, p_square_size, p_color_odd, p_color_even):
    for y in range(p_grid_size.y):
        for x in range(p_grid_size.x):
            start_pos = Vec2(x, y)
            start_pos.mult(p_square_size.x, p_square_size.y)

            if (y + x) % 2 != 0:
                pygame.draw.rect(p_window, p_color_even, start_pos.getPos() + p_square_size.getPos())
            else:
                pygame.draw.rect(p_window, p_color_odd, start_pos.getPos() + p_square_size.getPos())


def renderPawns(p_window, p_grid, p_grid_size, p_square_size, p_color_player_one, p_color_player_two,
                p_color_pawn_selected):
    for y in range(p_grid_size.y):
        for x in range(p_grid_size.x):
            square = p_grid[y][x]
            if square is not None:

                pos = Vec2(x, y)
                pos.mult(p_square_size.x, p_square_size.y)
                pos.x += p_square_size.x / 2
                pos.y += p_square_size.y / 2

                radius = p_square_size.x / 3

                if square.isSelected():
                    pygame.draw.circle(p_window, p_color_pawn_selected, pos.getPos(), radius)
                elif square.getPlayer() == 0:
                    pygame.draw.circle(p_window, p_color_player_one, pos.getPos(), radius)
                else:
                    pygame.draw.circle(p_window, p_color_player_two, pos.getPos(), radius)

                if square.getType() == 1:
                    pygame.draw.circle(p_window, (0, 0, 0), pos.getPos(), radius/2)


def renderMoves(p_window, p_board, p_square_size, p_color_pawn_selected):
    moves = p_board.getMoves()
    for move in moves:
        move = move[0]
        pos = Vec2(move.x, move.y)
        pos.mult(p_square_size.x, p_square_size.y)
        pos.x += p_square_size.x / 2
        pos.y += p_square_size.y / 2

        radius = p_square_size.x / 4

        pygame.draw.circle(p_window, p_color_pawn_selected, pos.getPos(), radius)
