from BasicVector import Vec2
from BasicVector.locals import *
from pawn import Pawn


class Board:

    def __init__(self, p_grid_size=Vec2(8, 8), p_square_size=Vec2(80, 80)):
        self.__grid_size = p_grid_size
        self.__square_size = p_square_size
        self.__grid = [[None for x in range(p_grid_size.x)] for y in range(p_grid_size.y)]
        self.__pawns_placement = ">0>>0>>0>>0/0>>0>>0>>0/>0>>0>>0>>0///1>>1>>1>>1/>1>>1>>1>>1/1>>1>>1>>1"
        self.__player = 0
        self.__pawn_selected = False
        self.__selected_pawn_pos = None
        self.__force_selection = False
        self.__moves = []
        self.__player_one_patterns = [Vec2(-1, 1), Vec2(1, 1)]
        self.__player_one_advanced_patterns = [Vec2(-2, 2), Vec2(2, 2)]
        self.__player_two_patterns = [Vec2(-1, -1), Vec2(1, -1)]
        self.__player_two_advanced_patterns = [Vec2(-2, -2), Vec2(2, -2)]
        self.__king_pawn_patterns = [Vec2(-1, 1), Vec2(1, 1), Vec2(-1, -1), Vec2(1, -1)]
        self.initPawns()

    def getGridSize(self):
        return self.__grid_size

    def getGrid(self):
        return self.__grid

    def getSquareSize(self):
        return self.__square_size

    def getMoves(self):
        return self.__moves

    def initPawns(self):
        x = 0
        y = 0
        for command in self.__pawns_placement:
            if command == "0" or command == "1":
                self.__grid[y][x] = Pawn(int(command), 0)

            if command == ">":
                x += 1

            if command == "/":
                y += 1
                x = 0

    def playerClicked(self, p_mouse_pos):

        index = self.getSquareIndexByMousePosition(p_mouse_pos)

        if self.__pawn_selected:
            # print("\nthe moves")
            for move in self.__moves:
                # print(move)
                if move[0] == index:
                    if self.__selected_pawn_pos is not None:
                        self.movePawn(self.__selected_pawn_pos, index)
                        # print(self.__moves)
                        # print(move)
                        # print("move ", index)
                        # print("eat ", move[1], "\n")

                        pawn = self.getPawnByPos(index)
                        if self.__player == 0 and index.y == self.__grid_size.y - 1 and pawn.getType() == 0:
                            pawn.setType(1)
                        elif self.__player == 1 and index.y == 0 and pawn.getType() == 0:
                            pawn.setType(1)

                        if move[1] is not None:
                            self.removePawnFrom(move[1])

                            if pawn.getType() == 0:
                                if self.hasAdvancedMoves(index, self.__player):
                                    self.__force_selection = True
                                else:
                                    self.__force_selection = False
                            else:
                                print(self.hasAdvancedKingMoves(index, self.__player))
                                if self.hasAdvancedKingMoves(index, self.__player):
                                    self.__force_selection = True
                                else:
                                    self.__force_selection = False

                        Pawn.resetSelection(self.__grid)
                        self.__moves = []
                        if not self.__force_selection:
                            self.__pawn_selected = False
                            self.__player += 1
                            self.__player %= 2
                        else:
                            self.__selected_pawn_pos = index
                            if self.getPawnByPos(index).getType() == 0:
                                self.__moves = self.findAdvancedMoves(index, self.__player, [0, 1])
                            else:
                                self.__moves = self.findAdvancedKingMoves(index, self.__player, [0, 1, 2, 3])

        if self.aPawnIsHere(index) and not self.__force_selection:
            pawn = self.getPawnByPos(index)
            if pawn.getPlayer() == self.__player:
                Pawn.resetSelection(self.__grid)
                pawn.setSelected(True)
                self.__pawn_selected = True
                self.__selected_pawn_pos = index
                self.__moves = self.findMovesOfAPawn(index)
                # print(self.__moves)

    def findMovesOfAPawn(self, p_index):
        pawn = self.getPawnByPos(p_index)
        player = pawn.getPlayer()
        moves = []
        if pawn.getType() == 0:
            moves = self.findMoves(p_index, player)
        else:
            moves = self.findKingMoves(p_index, player)

        return moves

    def findMoves(self, p_pos, p_player):
        moves = []

        if p_player == 0:
            for i, pattern in enumerate(self.__player_one_patterns):
                new_pos = Vec2(p_pos.x + pattern.x, p_pos.y + pattern.y)
                if not self.isOutside(new_pos):
                    if not self.aPawnIsHere(new_pos):
                        moves.append([new_pos, None])
                    else:
                        moves.extend(self.findAdvancedMoves(p_pos, p_player, [i]))
                        #print(moves)

        elif p_player == 1:
            for i, pattern in enumerate(self.__player_two_patterns):
                new_pos = Vec2(p_pos.x + pattern.x, p_pos.y + pattern.y)
                if not self.isOutside(new_pos):
                    if not self.aPawnIsHere(new_pos):
                        moves.append([new_pos, None])
                    else:
                        moves.extend(self.findAdvancedMoves(p_pos, p_player, [i]))
                        #print(moves)

        return moves

    def hasAdvancedMoves(self, p_pos, p_player):
        return len(self.findAdvancedMoves(p_pos, p_player, [0, 1])) != 0

    def findAdvancedMoves(self, p_pos, p_player, p_base_move):
        moves = []

        for i in p_base_move:
            if p_player == 0:
                pattern = self.__player_one_advanced_patterns[i]
                prec_pattern = self.__player_one_patterns[i]
                prec_pos = Vec2(p_pos.x + prec_pattern.x, p_pos.y + prec_pattern.y)
                if not self.isOutside(prec_pos):
                    if self.aPawnIsHere(prec_pos):
                        prec_pawn = self.getPawnByPos(prec_pos)
                        if prec_pawn.getPlayer() != p_player:
                            new_pos = Vec2(p_pos.x + pattern.x, p_pos.y + pattern.y)
                            if not self.isOutside(new_pos):
                                if not self.aPawnIsHere(new_pos):
                                    moves.append([new_pos, prec_pos])
                                    print(i, new_pos)

            elif p_player == 1:
                pattern = self.__player_two_advanced_patterns[i]
                prec_pattern = self.__player_two_patterns[i]
                prec_pos = Vec2(p_pos.x + prec_pattern.x, p_pos.y + prec_pattern.y)
                if not self.isOutside(prec_pos):
                    if self.aPawnIsHere(prec_pos):
                        prec_pawn = self.getPawnByPos(prec_pos)
                        if prec_pawn.getPlayer() != p_player:
                            new_pos = Vec2(p_pos.x + pattern.x, p_pos.y + pattern.y)
                            if not self.isOutside(new_pos):
                                if not self.aPawnIsHere(new_pos):
                                    moves.append([new_pos, prec_pos])
                                    print(i, new_pos)

        return moves

    def findKingMoves(self, p_pos, p_player):
        moves = []
        for i, pattern in enumerate(self.__king_pawn_patterns):
            new_pos = p_pos + pattern
            run = True
            while not self.isOutside(new_pos) and run:
                if not self.aPawnIsHere(new_pos):
                    moves.append([new_pos, None])
                else:
                    moves.extend(self.findAdvancedKingMoves(new_pos, p_player, [i]))
                    run = False

                new_pos += pattern

        return moves

    def hasAdvancedKingMoves(self, p_pos, p_player):
        return len(self.findAdvancedKingMoves(p_pos, p_player, [0, 1, 2, 3])) != 0

    def findAdvancedKingMoves(self, p_pos, p_player, p_last_move_dir):
        moves = []
        for i in p_last_move_dir:
            pattern = self.__king_pawn_patterns[i]

            if p_player != self.getPawnByPos(p_pos).getPlayer() or not self.aPawnIsHere(p_pos):
                print("hey")
                new_pos = p_pos + pattern
                run = True
                while not self.isOutside(new_pos) and run:
                    if not self.aPawnIsHere(new_pos):
                        moves.append([new_pos, p_pos])
                    else:
                        run = False
                    new_pos += pattern

        return moves

    def movePawn(self, p_from_pos, p_to_pos):
        # print("movePawn method from :", p_from_pos)
        # print("movePawn method to :", p_to_pos)
        self.__grid[p_to_pos.y][p_to_pos.x] = self.__grid[p_from_pos.y][p_from_pos.x]
        self.__grid[p_from_pos.y][p_from_pos.x] = None

    def getPawnByPos(self, p_index) -> Pawn:
        return self.__grid[p_index.y][p_index.x]

    def getSquareIndexByMousePosition(self, p_mouse_pos):
        return Vec2(p_mouse_pos[0] // self.__square_size.x, p_mouse_pos[1] // self.__square_size.y)

    def isOutside(self, p_pos):
        return not (ORIGIN_VEC2.x <= p_pos.x < self.__grid_size.x and ORIGIN_VEC2.y <= p_pos.y < self.__grid_size.y)

    def aPawnIsHere(self, p_pos):
        # print("aPawnIsHere method :", p_pos)
        return self.__grid[p_pos.y][p_pos.x] is not None

    def removePawnFrom(self, p_pos):
        self.__grid[p_pos.y][p_pos.x] = None
